(* Time-stamp: <modified the 31/01/2017 (at 14:21) by Erwan Jahier> *)

(* Recupere dans le log generé par getstat.r des infos interessantes  *)
(* #use "topfind";; *)
(* #require "str";; *)

let gen_cond str reg =  Str.string_match (Str.regexp reg) str 0

let get_min line = gen_cond line " Min[^0-9]+\\([0-9]+\\)"
let get_mean line = gen_cond line " Mean[^0-9]+\\([0-9]+\\)"
let get_median line = gen_cond line " Median[^0-9]+\\([0-9]+\\)"
let get_max line = gen_cond line " Max[^0-9]+\\([0-9]+\\)"
let get_error line = gen_cond line "ERROR: \\(.+\\)"


let get_wcet line = gen_cond line ".+The WCET computed by otawa is \\([0-9]+\\)"

let inc = stdin

let get1 = Str.matched_group 1
let filter str = 
  if Str.string_match (Str.regexp "This problem is \\(.+\\)$") str 0 then
    get1 str 
  else str
let get2 line = Str.matched_group 1 line, Str.matched_group 2 line

let min = ref ""
let max = ref ""
let mean = ref ""
let median = ref ""
let wcet = ref ""
let error = ref ""


let _ =
  try
    while true do
      let line = input_line inc in
      if get_min line then 
        min := get1 line
      else
        if get_max line then 
          max := get1 line
        else
          if get_median line then 
            median := get1 line
          else
            if get_mean line then 
              mean := get1 line
            else
              if get_wcet line then 
                wcet := get1 line
              else
                if get_error line then 
                  error := get1 line
                else
                  ()
    done

  with End_of_file ->
    let delta = try string_of_int ((int_of_string !wcet) - (int_of_string !max)) with _ ->""  in
    let delta_r= try float_of_string delta with _ -> 0.0 in
    let pourcent = delta_r *. 100.0 /. (float_of_string !wcet) in 
    Printf.printf "|=%s=|%s|%s|%s|%s|%s|%s|%.2f|%s| \n"  Sys.argv.(1) !min !median !mean  !max !wcet delta pourcent !error; 
    flush stderr;
    flush stdout
